package oszimt.helper;
import java.io.*;

public final class Input {
	
	private static String IntErrorMessage = "Es wird der Typ 'int' erwartet.";
	private static String FloatErrorMessage = "Es wird der Typ 'float' erwartet.";
	private static String DoubleErrorMessage = "Es wird der Typ 'double' erwartet.";
	
	/**
	 * 
	 * Gibt einen Abfragetext aus und gibt die Eingabe als Typ int zurück
	 * 
	 * @param text
	 * @return
	 */
	public static int Int(String text) {
		
		int value = 0;
		String readline;
		
		// zeige fragetext und lese zeile
		print(text); readline = readline();
				
		try {
			value = Integer.parseInt(readline);
		} catch (Exception e) {
			showErrorMsg(IntErrorMessage);
			value = Int(text);
		}
				
		return value;
	}
	
	public static void setIntErrorMessage(String message) {
		IntErrorMessage = message;
	}
	
	/**
	 * 
	 * Gibt einen Abfragetext aus und gibt die Eingabe als Typ float zurück
	 * 
	 * @param text
	 * @return
	 */
	public static float Float(String text) {
		
		float value = 0;
		String readline;
		
		// zeige fragetext und lese zeile
		print(text); readline = readline();
		
		try {
			value = Float.parseFloat(readline);
		} catch (Exception e) {
			showErrorMsg(FloatErrorMessage);
			value = Float(text);
		}
		
		return value;
	}
	
	public static void setFloatErrorMessage(String message) {
		FloatErrorMessage = message;
	}
	
	/**
	 * 
	 * Gibt einen Abfragetext aus und gibt die Eingabe als Typ double zurück
	 * 
	 * @param text
	 * @return
	 */
	public static double Double(String text) {
		
		double value = 0;
		String readline;
		
		// zeige fragetext und lese zeile
		print(text); readline = readline();
		
		try {
			value = Double.parseDouble(readline);
		} catch (Exception e) {
			showErrorMsg(DoubleErrorMessage);
			value = Double(text);
		}
		
		return value;
	}
	
	public static void setDoubleErrorMessage(String message) {
		DoubleErrorMessage = message;
	}
	
	/**
	 * 
	 * Gibt einen Abfragetext aus und gibt die Eingabe als Typ String zurück
	 * 
	 * @param text
	 * @return
	 */
	public static String String(String text) {
		String readline = null;
		
		// zeige fragetext und lese zeile
		print(text); readline = readline();
				
		return readline;
	}
	
	/**
	 * 
	 * Gibt einen Abfragetext aus und gibt die Eingabe als Typ char zurück
	 * 
	 * @param text
	 * @return char
	 */
	public static char Char(String text) {
		
		char value;
		String readline;
		
		// zeige fragetext und lese zeile
		print(text); readline = readline();
		
		value = readline.charAt(0);	
		
		return value;
	}
		
	/**
	 * 
	 * Bufferreader
	 * 
	 * @return String
	 */
	private static String readline() {
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String returnValue = null;
		
		try {
			returnValue = reader.readLine();
		} catch (IOException ioe) {
			showErrorMsg("Exception in Input Klasse. Beende Programm ...");
			showErrorMsg(ioe.getMessage());
			System.exit(1);
		}	      
		
		return returnValue;
	}
	
	/**
	 * 
	 * Fehlerausgabe
	 * 
	 * @param text
	 */
	private static void showErrorMsg(String text) {
		
		print(">> Fehler: ");
		print(text + "\n");
		
	}
	
	/**
	 * 
	 * Printanweisung
	 * 
	 * @param text
	 */
	private static void print(String text) {
		
		System.out.print(text);
		
	}
	
}
